## BLOC D'EXERCICIS PROGRAMACIÓ

---

#### Índex

- ##### [Exercici...1](#exercici-1)<br>
- ##### [Exercici...2](#exercici-2)<br>
- ##### [Exercici...3](#exercici-3)<br>
- ##### [Exercici...4](#exercici-4)<br>
- ##### [Exercici...5](#exercici-5)<br>
- ##### [Exercici...6](#exercici-6)<br>
- ##### [Exercici...7](#exercici-7)<br>
- ##### [Exercici...8](#exercici-8)<br>
- ##### [Exercici...9](#exercici-9)<br>
- ##### [Exercici...10](#exercici-10)<br>
- ##### [Exercici...11](#exercici-11)<br>
- ##### [Exercici...12](#exercici-12)<br> 
- ##### [Exercici...13](#exercici-13)<br>

---

#### Exercici 1

##### **Escriure un missatge de benvinguda per pantalla.**

<br>

Símplement he fet un printf d'un missatge de benvinguda.

```c
 printf("Bon dia!\n");
```
El resultat es veuria així:

![](Exercici_1.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 2

##### **Escriure un enter, un real, i un caràcter per pantalla.**

<br>

Aquí he fet un printf on hi introdueixo un valor enter (2), un valor real (3,14) i un caracter (c).
```c
 printf("Enter = %i\nReal = %.2lf\nCaracter = %c\n", 2, 3.14, 'c');
```
El resultat es veuria així:

![](Exercici_2.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici-3

##### **Demanar dos enters per teclat i mostrar la suma per pantalla.**

<br>

Per començar he declarat les variables.
```c
 //DECLARACIÓ DE LES VARIABLES
 int enter;
 int enter2;
```
<br>

Un cop declarades les variables, és necessari que l'usuari introdueixi per teclat els valors de les variables.
```c
 //INTRODUCCIÓ DE LES DADES PER TECLAT
 printf("Introdueix el primer enter: ");
 scanf("%i", &enter);
 printf("Introdueix el segon enter: ");
 scanf("%i", &enter2);
```

<br>

Per últim, només falta imprimir el resultat de la suma.
```c
 //RESULTAT
 printf("\nLa suma dels dos enters es: %i\n",
 enter+enter2);
```
El resultat es veuria així:

![](Exercici_3.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici-4

##### **Indicar el tipus de dades que correspon en cada cas.**

<br>

**Edat:**
Es tracta d'una dada de tipus ENTER
  
<br>

**Temperatura:**
Es tracta d'una dada de tipus REAL

<br>

**La resposta a la pregunta: Ets solter (s/n)?:**
Es tracta d'una dada de tipus CARÀCTER

<br>

**El resultat d’avaluar la següent expressió: (5>6 o 4<=8)**
Es tracta d'una dada de tipus BOOLEÀ

<br>

**El teu nom**
Es tracta d'una dada de tipus STRING

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 5

##### **Mostrar per pantalla els 5 primers nombres naturals.**

<br>

Simplement faig un printf dels 5 primers nombres naturals, l'1, el 2, el 3, el 4 i el 5.
```c
 printf("1, 2, 3, 4, 5\n");
```
El resultat es veuria així:

![](Exercici_5.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 6

##### **Mostrar per pantalla la suma i el producte dels 5 primers nombres naturals.**

<br>

Aqui he fet un printf de la suma i del producte dels 5 primers nombres naturals.
```c
 printf("Suma 5 primers nombres naturals: %i\n", 1+2+3+4+5);
 printf("Producte 5 primers nombres naturals: %i", 1*2*3*4*5);
```
El resultat es veuria així: 

![](Exercici_6.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 7

##### **Demanar dos enters i intercanviar els valors de les variables.**

<br>

Per començar he declarat les variables.
```c
 //DECLARACIÓ DE LES VARIABLES
 int enter;
 int enter2;
 int temporal; // (1)
```
>(1) *Aquesta variable emmagatzemarà temporalment el valor que hi hagi a la variable enter a l'hora de fer l'intercanvi, per tal que el seu valor no es perdi* 

<br>

Un cop declarades les variables, és necessari que l'usuari introdueixi per teclat els valors de les variables.
```c
 //INTRODUCCIÓ DE LES DADES PER TECLAT
 printf("Introdueix el primer enter: ");
 scanf("%i", &enter);
 printf("Introdueix el segon enter: ");
 scanf("%i", &enter2);
```

<br>

Ara que l'usuari ja ha entrat per teclat els valors de les variables, es hora d'imprimir els valors que ha entrat per pantalla.
```c
 //SORTIDA A PANTALLA DELS VALORS INTRODUITS
 printf("\nValor primer enter: %i\n", enter);
 printf("Valor segon enter: %i\n", enter2);
```

<br>

Seguidament, el que he fet ha sigut intercanviar els valors de les variables.
```c
 //INTERCANVI DE LES VARIABLES
 temporal=enter; // (2)
 enter=enter2;
 enter2=temporal;
```
>(2) *És molt important l'ordre dels factors, ja que aquí si que alteren el producte.*

<br>

I per últim, imprimir per pantalla els resultats de les variables amb els valors intercanviats.
```c
 //RESULTAT
 printf("\nNou valor primer enter: %i\n", enter);
 printf("Nou valor segon enter: %i\n", enter2);
```
El resultat es veuria així:

![](Exercici_7.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 8

##### Mostra per pantalla el següent menú:
  
***Opcions:*** <br>
&emsp;&emsp;***1-Alta*** <br>
&emsp;&emsp; ***2-Baixa*** <br>
&emsp;&emsp; ***3-Modificacions*** <br>
&emsp;&emsp; ***4-Sortir*** <br>
***Tria opció (1-4):***

<br>

Simplement he fet un printf per cada línia del menú.
```c
 printf("Opcions\n");
 printf("\t1-Alta\n");
 printf("\t2-Baixa\n");
 printf("\t3-Modificacions\n");
 printf("\t4-Sortir\n");
 printf("\nTria opcio (1-4):\n");
```
El resultat es veuria així:

![](Exercici_8.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 9

##### **Mostra per pantalla el següent menú:**

\************************ <br>
\**** &ensp;&ensp;&nbsp; MENU &ensp;&ensp;&nbsp; **** <br>
\************************ <br>
\**** &ensp;&ensp;&nbsp; 1-alta &ensp;&ensp;&ensp;&nbsp; **** <br>
\**** &ensp;&ensp;&nbsp; 2-baixa &ensp;&ensp;  **** <br>
\**** &ensp;&ensp;&nbsp; 3-sortir &ensp;&ensp;&nbsp; **** <br>
\************************

<br>

Al igual que en l'exercici anterior, he fet un printf per cada línia del menú
```c
 printf("*************************\n");
 printf("****      MENU       ****\n");
 printf("*************************\n");
 printf("****    1-alta       ****\n");
 printf("****    2-baixa      ****\n");
 printf("****    3-sortir     ****\n");
 printf("*************************\n");
```
El resultat es veuria així:

![](Exercici_9.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 10

##### **Fer un programa que permeti calcular el Capital Final *CF* d’una inversió, a interès simples, si sabem el Capital Inicial (*C$_0$*), el nombre d’anys (*n*), i el rèdit (*r*). <br><br>La fórmula per obtenir el CF a interès simples és <br> $CF = C_0 +\frac{C_0rn}{100*k}$**


<br>

Per començar he declarat les variables

```c
 //DECLARACIÓ DE LES VARIABLES
 int capitalInicial; // (3)
 int anys; 
 double redit;
```
>(3) *En aquestes variables s'hi emmagatzemarà els valors que l'usuari entri per teclat.*

<br>

A continuació li demano a l'usuari que entri els valors que vulgui introduir a cada variable per teclat.

```c
 //INTRODUCCIÓ DE LES DADES PER TECLAT
 printf("Introdueix el capital inicial: ");
 scanf("%i",&capitalInicial);
 printf("Introdueix el redit: ");
 scanf("%lf",&redit);
 printf("Introdueix els anys: ");
 scanf("%i",&anys);
```

<br>

Un cop l'usuari ha introduit els valors a les variables, es hora d'imprimir el resultat per pantalla.

```c
 //SORTIDA A PANTALLA DEL RESULTAT
 printf("El capital final es de %.2lf euros\n",
     capitalInicial+(capitalInicial*(redit/100)*anys)/100); // (4)
```

>(4) *Això és la fórmula que demanava l'enunciat.<br>Donat que el temps està expressat en anys la k=1*

El resultat es veuria així:

![](Exercici_10.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 11

##### **Fer un programa que permeti calcular el Capital Final *CF* d’una inversió, a interès compost, si sabem el Capital Inicial (*C$_0$*), el nombre d’anys (*n*), i el interès (*i*). <br><br>La fórmula per obtenir el CF a interès compost és <br> $CF = C_0 * (1+i)^n$**

<br>

Per començar he declarat les variables

```c
 //DECLARACIÓ DE LES VARIABLES
 int capitalInicial; // (5)
 int anys; 
 double interes;
```
>(5) *En aquestes variables s'hi emmagatzemarà els valors que l'usuari entri per teclat.*

<br>

A continuació li demano a l'usuari que entri els valors que vulgui introduir a cada variable per teclat.

```c
 //INTRODUCCIÓ DE LES DADES PER TECLAT
 printf("Introdueix el capital inicial: ");
 scanf("%i",&capitalInicial);
 printf("Introdueix els anys: ");
 scanf("%i",&anys);
 printf("Introdueix el interes: ");
 scanf("%lf",&interes);
```

<br>

Un cop l'usuari ha introduit els valors a les variables, es hora d'imprimir el resultat per pantalla.

```c
 //SORTIDA A PANTALLA DEL RESULTAT
 printf("\nEl capital final es de %.2lf euros\n", 
     capitalInicial*pow(1+(interes/100), anys)); // (6)
```

>(6) *Això és la fórmula que demanava l'enunciat.
Per poder utilitzar la funció pow (potència), és necessari utilitzar la llibreria <math.h>.*

El resultat es veuria així:

![](Exercici_11.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---


#### Exercici 12

##### **Fer un programa que permeti generar una travessa de forma aleatòria.**

<br>

Per començar he declarat les constants. 

```c
 #define BB while (getchar()!='\n') // (7)
 #define MAX 14 // (8)
 #define MAX_ALEATORI 3 // (9)
```
> (7)  *La primera constant que he declarat ha sigut el Buida Buffer, per poder entrar dades per teclat de forma correcta.*<br>

> (8)  *MAX_RANDOM indica el nombre aleatori màxim que vull que generi el programa, en aquest cas, 3.*<br>

> (9)   *MAX indica el nombre total de valors que l'usuari podrà entrar, en aquest cas, 14.*

<br>


Seguidament he declarat les variables que utilitzaré durant el programa. 

```c
 //DECLARACIÓ DE LES VARIABLES
 char entrada[MAX]; // (10)
 double aleatori[MAX]; // (11) 
 int index; // (12)
 int contador; // (13)
```
> (10) *A l'array de caracters entrada[MAX] s'emmagatzemarà cada valor que l'usuari introdueixi per teclat a cada partit.*<br>

> (11) *A l'array de doubles aleatori[MAX] s'emmagatzemarà cada valor que es generi aleatòriament per a cada partit.*<br>

> (12) *A la variable de tipus enter index s'emmagatzemarà la posició de l'index.*<br>

> (13) *A la variable de tipus enter contador s'emmagatzemarà el nombre d'encerts.*<br>

<br>

La següent funció és molt important introduirla, sense ella, el numero aleatori no es generaria. 

```c
 //FUNCIÓ RANDOM
 srand(getpid()); // (14)
```
>(14) *Per poder utilitzar aquesta funció és necessari utilitzar la llibreria <unistd.h>.*

<br>

Després de declarar totes les variables a utilitzar, les he inicialitzat.

```c
 //INICIALITZACIONS
 contador=0;
 index=0;
 while(index<MAX){ // (15)
     aleatori[index]=rand()%MAX_ALEATORI+1;
     index++; 
 } index = 0; // (16)
```

> (15) *Aquí es generarà a l'array aleatori un numero a l'atzar d'un valor màxim de MAX_ALEATORI+1 en la posició de l'array on estigui l'index en aquell moment. <br>Un cop generat, avançarà l'index una posició mentre index sigui inferior al nombre maxim de valors que es vol generar, es a dir, MAX.*

> (16) *Sempre que la variable index prengui per valor 0 serà perque la reutilitzaré mes endavant.*

<br>

Un cop declarades i inicialitzades les variables, és necessari que l'usuari introdueixi els valors de la seva travessa.

```c
 //INTRODUCCIÓ DE LES DADES PER TECLAT
 printf("Introdueix un dels seguents valors a cada partit (1/x/2):\n"); 
 while(index<MAX){ // (17)
     printf("\nPartit %i:  ", index+1);
     scanf("%c", &entrada[index]);BB; 
     if (entrada[index]!='1'&& // (18)
         entrada[index]!='x'&&
         entrada[index]!='2') {
           printf("Si us plau, introdueix un caracter valid!\n");
         } else index++;
 } index=0;
```
> (17) *Mentre index sigui inferior al numero màxim de valors que es vol introduir, es a dir, MAX, demana el valor que l'usuari vulgui  introduir en la posició de l'array entrada on estigui index.*

>(18)  *Si introdueix una dada amb un valor incorrecte, es a dir, que no sigui ni 1, ni x, ni 2, el programa no li deixarà entrar la dada, i li apareixerà el missatge introduit en el printf. 
Si la dada te un valor correcte, avançarà la posició d'index.*

La introducció dels valors es veuria així:

![](Exercici_11_1.jpg)

![](Exercici_11_2.jpg)

<br>

Ara que l'usuari ja ha entrat els valors de cada partit de la travessa per teclat, i s'han generat els valors aleatoris, es hora d'imprimir els resultats 

```c
 //SORTIDA A PANTALLA DELS RESULTATS
 printf("\nPartit   | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10| 11| 12| 13| 14|\n"); // (19)
 printf("---------|---------------------------------------------------------|\n"); // (20)
 printf("Aposta   |"); 
 while(entrada[index]!='\0'){ // (21)
     printf(" %c |", entrada[index]);
 index++;
 } index = 0;
 printf("\nResultat |"); 
 while (index<MAX){ // (22)
     if(aleatori[index]==3) printf( " x |"); // (23)
     else printf(" %.0lf |", aleatori[index]);
     index++;
 } index =0;
```

>(19) *Aquí sortirà el partit concret, i a sota de cada partit, l'aposta, es a dir, el valor introduit per l'usuari, i a sota de l'aposta, el resultat, es a dir, el valor aleatori generat per la màquina.*

>(20) *Això simplement és una linia discontínua per separar el número del partit de l'aposta i del resultat. Crec que queda bastant bé.*

>(21) *Aquí imprimirà el valor que l'usuari ha introduit prèviament.
Mentre el valor d'entrada d'index sigui diferent a un /0 imprimirà el valor que hi ha en aquella posició, i avançarà index a la següent posició.*

>(22) *Aquí imprimirà el valor que la màquina ha generat a l'atzar previament mentre el valor d'aleatori d'index sigui diferent de MAX.*

>(23) *Si el número generat és un 3, el caràcter que imprimirà serà una x.
Del contrari, si és un 1 o un 2, imprimirà el número generat. 
Al final, avançarà index una posició.*

Els resultats de cada partit es veurien així:

![](Exercici_11_3.jpg)

<br>

Ara només falta imprimir a pantalla quants resultats ha encertat.

```c
 //NÚMERO D'ENCERTS
 while (index<MAX){ // (24)
     if ((entrada[index]=='1'&&aleatori[index]==1)||
         (entrada[index]=='2'&&aleatori[index]==2)||
         (entrada[index]=='x'&&aleatori[index]==3))contador++;
     index++;
 }
 printf("\n\nN'has encertat %i\n",contador); // (25)

 if (contador==14) printf("Enhorabona!!!! t'ha tocat el premi!!!!!!!\n"); // (26)
 else if (contador==13) printf("Quina llastima, no t'ha tocat PER UN. Aixo si que es tenir mala sort!");
 else if (contador<13&&contador>0) printf("Quina llastima, no t'ha tocat\n") ;
 else if (contador==0) printf("Ostres, no n'has encertat ni una\n");
```
>(24) *Mentre index sigui mes petit que MAX, si el valor entrat és el mateix que el valor generat,
sumarà 1 al contador.
Tot seguit avançarà index una posició*

>(25) *Aquí imprimirà el total d'encerts*

>(26) *Depenent del número d'encerts, imprimirà per pantalla un missatge o un altre.*

El nombre de resultats que ha encertat es veuria així:

![](Exercici_11_4.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---

#### Exercici 13 

##### **Fer un programa que simuli tirar un dau.**

<br>

Per començar he declarat les constants.

```c
#define MAX 6 // (27)
```
>(27) *MAX indica el nombre aleatori màxim que vull que generi el programa, en aquest cas, 3.* 


<br>

Seguidament he declarat les variables que utilitzaré durant el programa.

```c
 //DECLARACIÓ DE LES VARIABLES
 int entrada ; // (28)
 double aleatori ; //(29)
```

>(28) *A la variable de tipus enter entrada s'emmagatzemarà el valor que l'usuari creu que sortirà al "tirar" el dau.*

>(29) *A la variable de tipus enter aleatori s'emmagatzemarà el valor que la màquina generi aleatòriament per tal de simular la tirada d'un dau.*

<br>

La següent funció és molt important introduirla, sense ella, el numero aleatori no es generaria. 

```c
 //FUNCIÓ RANDOM
 srand(getpid()); // (30)
```

>(30) *Per poder utilitzar aquesta funció és necessari utilitzar la llibreria <unistd.h>.*

<br>

Després de declarar totes les variables a utilitzar i les he inicialitzat.

```c
 //INICIALITZACIONS
 aleatori=rand()%MAX+1; //(31)
```

>(31) *Aquí es generarà a la variable aleatori un número a l'atzar d'un valor màxim de MAX+1.*

<br>

Un cop declarades i inicialitzades les variables, és necessari que l'usuari introdueixi el valor que creu que sortirà al dau.

```c
 //INTRODUCCIÓ DE LES DADES PER TECLAT
 printf("Quin numero creus que et sortira al dau (1-6)? ");
 scanf("%i", &entrada);
```

<br>

Ara que l'usuari ja ha entrat el valor que creu que sortirà al dau per teclat i s'ha generat el valor aleatori del dau, es hora d'imprimir el resultat.

```c
 //SORTIDA A PANTALLA DEL RESULTAT
 printf("\nHas triat el %i\n", entrada);
 printf("Ha sortit el %.0lf\n", aleatori);
```

<br>

Ara només falta comprovar si l'ha encertat.
```c
 //COMPROVAR SI L'HA ENCERTAT
 if(entrada==aleatori) printf("\nENHORABONA!!! L'has encertat\n");  //(32)
 else printf("\nQuina llastima! No l'has encertat\n");
```

>(32) *Si el valor entrat és igual que el valor generat a l'atzar, li dirà que l'ha encertat.
Si no, li dirà que no l'ha encertat.*

El resultat es veuria així: 

![](Exercici_12_1.jpg)

![](Exercici_12_2.jpg)

<div style="text-align: right">

[Tornar a l'índex...](#índex)

</div>

---
