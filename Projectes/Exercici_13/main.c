#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 6

int main()
{
    //DECLARACI� DE LES VARIABLES
    int entrada ;
    double aleatori ;

    //FUNCI� RANDOM
    srand(getpid());

    //INICIALITZACIONS
    aleatori=rand()%MAX+1;

    //INTRODUCCI� DE LES DADES PER TECLAT
    printf("Quin numero creus que et sortira al dau (1-6)? ");
    scanf("%i", &entrada);

    //SORTIDA A PANTALLA DEL RESULTAT
    printf("\nHas triat el %i\n", entrada);
    printf("Ha sortit el %.0lf\n", aleatori);

    //COMPROVAR SI L'HA ENCERTAT
    if(entrada==aleatori) printf("\nENHORABONA!!! L'has encertat\n");
    else printf("\nQuina llastima! No l'has encertat\n");

    return 0;
}
