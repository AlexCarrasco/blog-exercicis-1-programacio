#include <stdio.h>
#include <stdlib.h>

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int capitalInicial;
    int anys;
    double redit;

    //INTRODUCCIÓ DE LES DADES PER TECLAT
    printf("Introdueix el capital inicial: ");
    scanf("%i",&capitalInicial);
    printf("Introdueix el redit: ");
    scanf("%lf",&redit);
    printf("Introdueix els anys: ");
    scanf("%i",&anys);

     //SORTIDA A PANTALLA DEL RESULTAT
    printf("\nEl capital final es de %.2lf euros\n", capitalInicial+(capitalInicial*(redit/100)*anys)/100);


    return 0;
}
