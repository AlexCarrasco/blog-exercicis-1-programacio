#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int capitalInicial;
    int anys;
    double interes;

    //INTRODUCCIÓ DE LES DADES PER TECLAT
    printf("Introdueix el capital inicial: ");
    scanf("%i",&capitalInicial);
    printf("Introdueix els anys: ");
    scanf("%i",&anys);
    printf("Introdueix el interes: ");
    scanf("%lf",&interes);

    //SORTIDA A PANTALLA DEL RESULTAT
    printf("\nEl capital final es de %.2lf euros\n", capitalInicial*pow(1+(interes/100), anys));

    return 0;
}
