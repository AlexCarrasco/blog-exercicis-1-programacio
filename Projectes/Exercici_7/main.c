#include <stdio.h>
#include <stdlib.h>

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int enter;
    int enter2;
    int temporal;

    //INTRODUCCIÓ DE LES DADES PER TECLAT
    printf("Introdueix el primer enter: ");
    scanf("%i", &enter);
    printf("Introdueix el segon enter: ");
    scanf("%i", &enter2);

    //SORTIDA A PANTALLA DELS VALORS INTRODUITS
    printf("\nValor primer enter: %i\n", enter);
    printf("Valor segon enter: %i\n", enter2);

    //INTERCANVI DE LES VARIABLES
    temporal=enter;
    enter=enter2;
    enter2=temporal;

    //RESULTAT
    printf("\nNou valor primer enter: %i\n", enter);
    printf("Nou valor segon enter: %i\n", enter2);

    return 0;
}
