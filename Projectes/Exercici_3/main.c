#include <stdio.h>
#include <stdlib.h>

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    int enter;
    int enter2;

    //INTRODUCCIÓ DE LES DADES PER TECLAT
    printf("Introdueix el primer enter: ");
    scanf("%i", &enter);
    printf("Introdueix el segon enter: ");
    scanf("%i", &enter2);

    //RESULTAT
    printf("\nLa suma dels dos enters es: %i\n",
    enter+enter2);

    return 0;
}
