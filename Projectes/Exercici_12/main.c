#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BB while (getchar()!='\n')
#define MAX 14
#define MAX_ALEATORI 3

int main()
{
    //DECLARACIÓ DE LES VARIABLES
    char entrada[MAX];
    double aleatori[MAX];
    int index;
    int contador;

    //FUNCIÓ RANDOM
    srand(getpid());

    //INICIALITZACIONS
    contador=0;
    index=0;
    while(index<MAX){
        aleatori[index]=rand()%MAX_ALEATORI+1;
        index++;
    } index = 0;

    //INTRODUCCIÓ DE LES DADES PER TECLAT
    printf("Introdueix un dels seguents valors a cada partit (1/x/2):\n");
    while(index<MAX){
        printf("\nPartit %i:  ", index+1);
        scanf("%c", &entrada[index]);BB;
        if (entrada[index]!='1'&&
        entrada[index]!='x'&&
        entrada[index]!='2') {
            printf("\nSi us plau, introdueix un caracter valid!\n");
        } else index++;
    } index=0;

    //SORTIDA A PANTALLA DELS RESULTATS
    printf("\nPartit   | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10| 11| 12| 13| 14|\n");
    printf("---------|-------------------------------------------------------|\n");
    printf("Aposta   |");
    while(entrada[index]!='\0'){
        printf(" %c |", entrada[index]);
        index++;
    } index = 0;
    printf("\nResultat |");
    while (index<MAX){
        if(aleatori[index]==3) printf( " x |");
        else printf(" %.0lf |", aleatori[index]);
    index++;
    } index =0;

    //NÚMERO D'ENCERTS
    while (index<MAX){
        if ((entrada[index]=='1'&&aleatori[index]==1)||
            (entrada[index]=='2'&&aleatori[index]==2)||
            (entrada[index]=='x'&&aleatori[index]==3))contador++;
        index++;
    }
    printf("\n\nN'has encertat %i\n",contador);
    if (contador==14) printf("Enhorabona!!!! t'ha tocat el premi!!!!!!!\n");
    else if (contador==13) printf("Quina llastima, no t'ha tocat PER UN. Aixo si que es tenir mala sort!");
    else if (contador<13&&contador>0) printf("Quina llastima, no t'ha tocat\n") ;
    else if (contador==0) printf("Ostres, no n'has encertat ni una\n");

    return 0;
}
